from typing import Dict, List

import graphene
import sqlalchemy
from graphene.relay.connection import PageInfo
from graphene_sqlalchemy import SQLAlchemyConnectionField
from graphql_relay import from_global_id
from graphql_relay.connection.arrayconnection import connection_from_list_slice
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm.query import Query

from .converter import convert_hybrid_property_return_type, convert_sqlalchemy_type
from .utils import camel_case_to_underscore


class FilterSQLAlchemyConnectionField(SQLAlchemyConnectionField):
    def __init__(self, type, *args, **kwargs):
        model = type._meta.model
        kwargs.update(get_model_filtering_fields(model))
        kwargs.update(get_model_ordering_fields(model))
        super().__init__(type, *args, **kwargs)

    @classmethod
    def get_query(cls, model, info, **args):
        return get_query(model, info, **args)

    @classmethod
    def connection_resolver(cls, resolver, connection, model, root, info, **args):
        iterable = resolver(root, info, **args)
        if iterable is None:
            iterable = cls.get_query(model, info, **args)
        else:
            if args:
                iterable = filter_query(iterable, model, info, **args)

        if isinstance(iterable, Query):
            _len = iterable.count()
        else:
            _len = len(iterable)
        return connection_from_list_slice(
            iterable,
            args,
            slice_start=0,
            list_length=_len,
            list_slice_length=_len,
            connection_type=connection,
            pageinfo_type=PageInfo,
            edge_type=connection.Edge,
        )


def get_query(model, info, **args):
    query = getattr(model, "query", None)
    if not query:
        session = info.context.get("session")
        if not session:
            raise Exception(
                "A query in the model Base or a session in the schema is required for querying.\n"
                "Read more http://docs.graphene-python.org/projects/sqlalchemy/en/latest/tips/#querying"
            )
        query = session.query(model)
    query = filter_query(query, model, info, **args)
    return query


def filter_query(query, model, info, **args):
    filterable_fields = []
    if hasattr(model, "__filterable__"):
        filterable_fields = getattr(model, "__filterable__")

    filter_args = get_filtering_args(model, filterable_fields, args)
    order_args = get_ordering_args(model, filterable_fields, args)
    return query.filter(*filter_args).order_by(*order_args)


def get_filtering_args(model, filterable_fields: List, args: Dict) -> List:
    filter_args = []
    for attribute_name, attribute_value in args.items():
        if attribute_name in filterable_fields:
            if attribute_name == "gid":
                attribute_name = "id"
                attribute_value = from_global_id(attribute_value)
            model_attribute = getattr(model, attribute_name)

            if isinstance(attribute_value, str):
                if len(attribute_value) < 1:
                    continue
                if _like_string(attribute_value):
                    filter_args.append(model_attribute.ilike(attribute_value))
                else:
                    if "&" in attribute_value:
                        conjunction_values = attribute_value.split("&")
                        conjunction_args = [
                            model_attribute == value.strip()
                            for value in conjunction_values
                        ]
                        filter_args.append(sqlalchemy.and_(*conjunction_args))
                    else:
                        alternative_values = attribute_value.split("|")
                        alternative_args = [
                            model_attribute == value.strip()
                            for value in alternative_values
                        ]
                        filter_args.append(sqlalchemy.or_(*alternative_args))
            else:
                filter_args.append(model_attribute == attribute_value)

    return filter_args


def get_ordering_args(model, filterable_fields: List, args: Dict) -> List:
    order_args = []
    for order_arg in args.get("orderBy", []):
        # examplary order_string: modified__DESC
        underscore_arg = camel_case_to_underscore(order_arg)
        attribute_name, direction = get_order_field_and_direction(underscore_arg)
        if attribute_name in filterable_fields and hasattr(model, attribute_name):
            model_attribute = getattr(model, attribute_name)
            if direction == "desc":
                order_args.append(model_attribute.desc().nullslast())
            else:
                order_args.append(model_attribute.asc().nullslast())
    return order_args


def get_order_field_and_direction(input_field):
    directions_table = ["asc", "desc"]
    direction = "desc"
    field = ""
    if isinstance(input_field, str):
        result = input_field.split("__")
        if len(result) > 1:
            direction = result[1]
            if direction.lower() not in directions_table:
                direction = "desc"
        field = result[0]
    return field, direction


def get_model_filtering_fields(model):
    args = {}
    fields = []
    if hasattr(model, "__filterable__"):
        fields.extend(getattr(model, "__filterable__"))
    if hasattr(model, "__custom_resolver_args__"):
        fields.extend(getattr(model, "__custom_resolver_args__"))
    for name in fields:
        attribute = getattr(model, name)
        if hasattr(attribute, "descriptor") and isinstance(
            attribute.descriptor, hybrid_property
        ):
            args[name] = convert_hybrid_property_return_type(attribute)
        else:
            args[name] = convert_sqlalchemy_type(
                getattr(attribute, "type", None), attribute
            )
    return args


def get_model_ordering_fields(model):
    args = {}
    if hasattr(model, "__filterable__"):
        args["orderBy"] = graphene.List(graphene.String)
    return args


def _like_string(string):
    if string.startswith("%") or string.endswith("%"):
        return True
    return False
