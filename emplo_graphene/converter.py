import datetime
import sys
import uuid
from decimal import Decimal
from functools import singledispatch
from typing import Any, Callable, Optional, Union

import graphene
from graphene.types.json import JSONString
from graphene_sqlalchemy.enums import enum_for_sa_enum
from graphene_sqlalchemy.registry import get_global_registry
from sqlalchemy import types
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import MapperProperty

ChoiceType = JSONType = ScalarListType = TSVectorType = object


def get_column_doc(column):
    return getattr(column, "doc", None)


def is_column_nullable(column):
    return bool(getattr(column, "nullable", True))


def is_union(type_arg: Any, **kwargs) -> bool:
    if sys.version_info >= (3, 10):
        from types import UnionType

        if isinstance(type_arg, UnionType):
            return True
    return getattr(type_arg, "__origin__", None) == Union


def convert_hybrid_property_return_type(hybrid_prop: hybrid_property) -> Callable:
    return_type_annotation = hybrid_prop.fget.__annotations__.get("return", None)
    if not return_type_annotation:
        raise TypeError(
            "Cannot convert hybrid property type {} to a valid graphene type. "
            "Please make sure to annotate the return type of the hybrid property or use the "
            "type_ attribute of ORMField to set the type.".format(hybrid_prop)
        )
    if is_union(return_type_annotation):
        return convert_sqlalchemy_hybrid_property_union(return_type_annotation)
    return convert_sqlalchemy_type(return_type_annotation(), column=hybrid_prop)


def convert_sqlalchemy_hybrid_property_union(type_arg: Any, **kwargs):
    nested_types = list(filter(lambda x: type(None) is not x, type_arg.__args__))
    graphene_types = [
        convert_sqlalchemy_type(nested_type(), column=None, **kwargs)
        for nested_type in nested_types
    ]

    if len(graphene_types) == 1:
        return graphene_types[0]

    if not all(
        isinstance(graphene_type, type(graphene.ObjectType))
        for graphene_type in graphene_types
    ):
        raise ValueError(
            "Cannot convert hybrid_property Union to graphene.Union: the Union contains scalars. "
            "Please add the corresponding hybrid_property to the excluded fields in the ObjectType, "
            "or use an ORMField to override this behaviour."
        )

    obj_types = list(
        filter(
            lambda x: isinstance(x, type(graphene.ObjectType)),
            graphene_types,
        )
    )
    union_name = "".join(sorted(obj_type._meta.name for obj_type in obj_types))
    union_type = graphene.Union.create_type(union_name, types=obj_types)
    return union_type


@singledispatch
def convert_sqlalchemy_type(
    type_arg: Any,
    column: Optional[Union[MapperProperty, hybrid_property]] = None,
    **kwargs,
):
    raise TypeError(
        "Don't know how to convert the SQLAlchemy field %s (%s, %s). "
        "Please add a type converter or set the type manually using ORMField(type_=your_type)"
        % (column, column.__class__ or "no column provided", type_arg)
    )


@convert_sqlalchemy_type.register(graphene.ObjectType)
def convert_object_type(type_arg, column, **kwargs):
    return type_arg


@convert_sqlalchemy_type.register(graphene.Scalar)
def convert_scalar_type(type_arg, column, **kwargs):
    return type_arg


@convert_sqlalchemy_type.register(str)
@convert_sqlalchemy_type.register(types.String)
@convert_sqlalchemy_type.register(types.Text)
@convert_sqlalchemy_type.register(types.Unicode)
@convert_sqlalchemy_type.register(types.UnicodeText)
@convert_sqlalchemy_type.register(postgresql.INET)
@convert_sqlalchemy_type.register(postgresql.CIDR)
def convert_column_to_string(type_arg, column, **kwargs):
    return graphene.String()


@convert_sqlalchemy_type.register(postgresql.UUID)
@convert_sqlalchemy_type.register(uuid.UUID)
def convert_column_to_uuid(
    type_arg: Any,
    column,
    **kwargs,
):
    return graphene.UUID()


@convert_sqlalchemy_type.register(types.DateTime)
@convert_sqlalchemy_type.register(datetime.datetime)
def convert_column_to_datetime(
    type_arg: Any,
    column,
    **kwargs,
):
    return graphene.DateTime()


@convert_sqlalchemy_type.register(types.Time)
@convert_sqlalchemy_type.register(datetime.time)
def convert_column_to_time(
    type_arg: Any,
    column,
    **kwargs,
):
    return graphene.Time()


@convert_sqlalchemy_type.register(types.Date)
@convert_sqlalchemy_type.register(datetime.date)
def convert_column_to_date(
    type_arg: Any,
    **kwargs,
):
    return graphene.Date()


@convert_sqlalchemy_type.register(int)
@convert_sqlalchemy_type.register(types.SmallInteger)
@convert_sqlalchemy_type.register(types.Integer)
def convert_column_to_int_or_id(
    type_arg,
    column,
    **kwargs,
):
    if column is not None:
        if getattr(column, "primary_key", False) is True:
            return graphene.ID()
    return graphene.Int()


@convert_sqlalchemy_type.register(types.Boolean)
@convert_sqlalchemy_type.register(bool)
def convert_column_to_boolean(
    type_arg,
    column,
    **kwargs,
):
    return graphene.Boolean()


@convert_sqlalchemy_type.register(float)
@convert_sqlalchemy_type.register(types.Float)
@convert_sqlalchemy_type.register(types.Numeric)
@convert_sqlalchemy_type.register(types.BigInteger)
def convert_column_to_float(
    type_arg,
    column,
    **kwargs,
):
    return graphene.Float()


@convert_sqlalchemy_type.register(postgresql.ENUM)
@convert_sqlalchemy_type.register(types.Enum)
def convert_enum_to_enum(
    type_arg: Any,
    column,
    **kwargs,
):
    if column is None or isinstance(column, hybrid_property):
        raise Exception("SQL-Enum conversion requires a column")

    return lambda: enum_for_sa_enum(column.type, get_global_registry())


def init_array_list_recursive(inner_type, n):
    return (
        inner_type
        if n == 0
        else graphene.List(init_array_list_recursive(inner_type, n - 1))
    )


@convert_sqlalchemy_type.register(types.ARRAY)
@convert_sqlalchemy_type.register(postgresql.ARRAY)
def convert_array_to_list(
    type_arg: Any,
    column,
    **kwargs,
):
    if column is None or isinstance(column, hybrid_property):
        raise Exception("SQL-Array conversion requires a column")
    item_type = column.type.item_type
    if not isinstance(item_type, type):
        item_type = type(item_type)
    inner_type = convert_sqlalchemy_type(item_type, column=column, **kwargs)
    return graphene.List(
        init_array_list_recursive(inner_type, (column.type.dimensions or 1) - 1)
    )


@convert_sqlalchemy_type.register(postgresql.HSTORE)
@convert_sqlalchemy_type.register(postgresql.JSON)
@convert_sqlalchemy_type.register(postgresql.JSONB)
def convert_json_to_string(
    type_arg: Any,
    column,
    **kwargs,
):
    return JSONString()


@convert_sqlalchemy_type.register(types.JSON)
def convert_json_type_to_string(
    type_arg: Any,
    column,
    **kwargs,
):
    return JSONString()


@convert_sqlalchemy_type.register(types.Variant)
def convert_variant_to_impl_type(
    type_arg,
    column,
    **kwargs,
):
    if column is None or isinstance(column, hybrid_property):
        raise Exception("Vaiant conversion requires a column")

    type_impl = column.type.impl
    if not isinstance(type_impl, type):
        type_impl = type(type_impl)
    return convert_sqlalchemy_type(type_impl, column=column, **kwargs)


@convert_sqlalchemy_type.register(Decimal)
def convert_sqlalchemy_hybrid_property_type_decimal(type_arg: Any, **kwargs):
    return graphene.String()
