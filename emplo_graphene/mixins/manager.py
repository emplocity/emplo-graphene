from typing import TYPE_CHECKING, Any, Callable, ClassVar, Optional

from ..utils import retrieve_id

Model = Any


class GrapheneManagerMixin:
    model: ClassVar[Model]

    if TYPE_CHECKING:
        get: Callable[[int], Optional[Model]]

    def get_by_gid(self, gid: str) -> Optional[Model]:
        object_id = retrieve_id(gid, self.model.__name__)
        return self.get(object_id)
