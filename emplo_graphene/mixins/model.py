from graphql_relay import to_global_id
from sqlalchemy.orm import Mapped


class GrapheneModelMixin:
    id: Mapped[int]

    @property
    def gid(self) -> str:
        return to_global_id(self.__class__.__name__, self.id)
