import json
import logging

from graphene_file_upload.utils import place_files_in_operations
from graphql import Source, execute, get_operation_ast, parse, validate
from graphql.error import GraphQLError
from graphql.error import format_error as format_graphql_error
from graphql.execution import ExecutionResult
from graphql.type.schema import GraphQLSchema
from werkzeug.exceptions import BadRequest, MethodNotAllowed
from werkzeug.wrappers import Response

try:
    import sentry_sdk
except ImportError:
    sentry_sdk = None  # type: ignore[assignment]


class HttpError(Exception):
    def __init__(self, response, message=None, *args, **kwargs):
        self.response = response
        self.message = message = message or response.description
        super().__init__(message, *args, **kwargs)


class GraphQLEndpoint:
    root_value = None

    def __init__(self, schema, middleware=None, get_identity=None):
        """
        :param schema:
        :param middleware:
        :param get_identity(request, **kwargs): custom defined function to authenticate user, must get request and
            kwargs as arguments and authenticate user using session or repository.
        """

        self.schema = schema
        self.middleware = middleware
        self.get_identity = get_identity
        self.logger = logging.getLogger(__name__)
        inner_schema = getattr(self.schema, "schema", None)

        self.executor = getattr(self.schema, "executor", None)

        if inner_schema:
            self.schema = inner_schema

        assert isinstance(self.schema, GraphQLSchema)

    def execute(self, *args, **kwargs):
        if self.middleware:
            kwargs["middleware"] = self.middleware
        return execute(self.schema, *args, **kwargs)

    def _get_context(self, request, session, repositories, rpc_proxies, **kwargs):
        context = {
            "user": (
                self.get_identity(request, session=session, repositories=repositories)
                if self.get_identity
                else None
            ),
            "request": request,
            "session": session,
            "repositories": repositories,
            "rpc_proxies": rpc_proxies,
        }
        context.update(**kwargs)
        return context

    def parse_body(self, request):
        content_type = request.mimetype
        if content_type == "application/graphql":
            return {"query": request.data.decode("utf-8")}

        elif content_type == "application/json":
            try:
                request_json = json.loads(request.data.decode("utf-8"))
                assert isinstance(request_json, dict)
                return request_json
            except Exception:
                raise HttpError(BadRequest("POST body sent invalid JSON."))

        elif content_type == "application/x-www-form-urlencoded":
            return request.form

        elif content_type == "multipart/form-data":
            operations = json.loads(request.form.get("operations", "{}"))
            files_map = json.loads(request.form.get("map", "{}"))
            operations = place_files_in_operations(operations, files_map, request.files)
            return operations
        return {}

    def execute_graphql_request(self, context, query, variables, operation_name):
        try:
            source = Source(query, name="GraphQL request")
            ast = parse(source)
            validation_errors = validate(self.schema, ast)
            if validation_errors:
                self.logger.error(f"Query validation errors: {validation_errors}")
                return ExecutionResult(errors=validation_errors, invalid=True)
        except Exception as e:
            self.logger.exception("Query parsing error")
            return ExecutionResult(errors=[e], invalid=True)

        operation_ast = get_operation_ast(ast, operation_name)
        if context.get("request").method.lower() == "get":
            if operation_ast and operation_ast.operation != "query":
                raise HttpError(
                    MethodNotAllowed(
                        ["POST"],
                        f"Can only perform a {operation_ast.operation} operation from a POST request.",
                    )
                )
        try:
            if operation_ast:
                name = operation_ast.name.value if operation_ast.name else "<unnamed>"
                self.logger.debug(f"Executing {operation_ast.operation}: {name}")
            else:
                self.logger.warning(f"No operation found in {ast}")
            return self.execute(
                ast,
                root_value=self.root_value,
                variable_values=variables or {},
                operation_name=operation_name,
                context_value=context,
                executor=self.executor,
            )
        except Exception as e:
            self.logger.exception("Query execution error")
            return ExecutionResult(errors=[e], invalid=True)

    def query(
        self, request, session=None, repositories=None, rpc_proxies=None, **kwargs
    ):
        try:
            if request.method.lower() not in ("post", "get"):
                raise HttpError(
                    MethodNotAllowed(
                        ["POST", "GET"], "GraphQL only supports POST requests."
                    )
                )

            data = self.parse_body(request)

            query, variables, operation_name = self.get_graphql_params(request, data)

            context = self._get_context(
                request, session, repositories, rpc_proxies, **kwargs
            )

            execution_result = self.execute_graphql_request(
                context, query, variables, operation_name
            )

            status_code = 400
            if execution_result:
                response = {}

                if execution_result.errors:
                    response["errors"] = [
                        self.format_error(e) for e in execution_result.errors
                    ]

                if not execution_result.invalid:
                    status_code = 200
                    response["data"] = execution_result.data

                result = self.json_encode(response)
            else:
                result = None

            return Response(
                status=status_code, response=result, content_type="application/json"
            )

        except HttpError as e:
            return Response(
                self.json_encode({"errors": [self.format_error(e)]}),
                status=e.response.code,
                headers={"Allow": ["GET, POST"]},
                content_type="application/json",
            )

    def json_encode(self, d):
        return json.dumps(d, sort_keys=True, indent=2, separators=(",", ": "))

    def get_sentry_event_id(self, error):
        if not sentry_sdk:
            self.logger.error("sentry-sdk is not installed")
            return None

        if isinstance(error, GraphQLError) and hasattr(error, "original_error"):
            event_id = sentry_sdk.capture_exception(error.original_error)
        else:
            event_id = sentry_sdk.capture_exception(error)
        return event_id

    def format_error(self, error):
        event_id = self.get_sentry_event_id(error)

        if isinstance(error, GraphQLError) and hasattr(error, "original_error"):
            formated_error = format_graphql_error(error)
            formated_error["message"] = error.original_error.__class__.__name__
            formated_error["sentry"] = str(event_id)
            formated_error["error_type"] = error.original_error.__class__.__name__
            return formated_error

        return {
            "message": "Internal server error",
            "sentry": str(event_id),
            "error_type": "InternalError",
        }

    @staticmethod
    def get_graphql_params(request, data):
        query = request.args.get("query") or data.get("query")
        variables = request.args.get("variables") or data.get("variables")

        if variables and isinstance(variables, str):
            try:
                variables = json.loads(variables)
            except Exception as e:
                raise HttpError(BadRequest("Variables are invalid JSON.")) from e

        operation_name = request.args.get("operationName") or data.get("operationName")

        return query, variables, operation_name
