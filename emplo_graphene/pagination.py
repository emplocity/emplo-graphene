from typing import List, Optional, Type

from graphene import ObjectType
from graphene.relay import Connection, PageInfo
from graphql_relay import from_global_id, to_global_id


class RelayPagination:
    """
    Args:
        connection_type: graphene relay connection object type
        queryset_size (int): number of elements in dataset
        first (int): limit the number of results
        after (string): id of the cursor in connection.edges list
                        when set, you return only results after the cursor position
    """

    def __init__(
        self,
        connection_type: Type[Connection],
        queryset_size: int,
        first: Optional[int] = None,
        after: Optional[str] = None,
    ):
        self.connection_type = connection_type
        self.queryset_size = queryset_size
        self.first = first
        self._after = after

    def _from_global_id(self, gid: Optional[str]) -> int:
        try:
            return int(from_global_id(gid)[1])
        except (TypeError, IndexError):
            raise

    @classmethod
    def get_cursor(cls, index: int) -> str:
        return to_global_id("arrayconnection", index)

    @property
    def after(self) -> int:
        return self._from_global_id(self._after)

    @property
    def offset(self) -> int:
        if self._after:
            return self.after + 1
        return 0

    @property
    def limit(self) -> Optional[int]:
        if self.first:
            return self.first
        return None

    def get_connection(self, nodes: List[ObjectType]) -> Connection:
        if self.limit and self.limit < self.queryset_size:
            size = self.limit
        else:
            size = self.queryset_size

        has_next_page = False
        has_previous_page = False
        if self.offset + size < self.queryset_size:
            has_next_page = True
        if self.offset > 0:
            has_previous_page = True
        page_info = PageInfo(
            start_cursor=self.get_cursor(self.offset),
            end_cursor=self.get_cursor(self.offset + size - 1),
            has_previous_page=has_previous_page,
            has_next_page=has_next_page,
        )

        edges = []
        for index, node in enumerate(nodes):
            edges.append(
                self.connection_type.Edge(
                    node=node, cursor=self.get_cursor(self.offset + index)
                )
            )
        return self.connection_type(edges=edges, page_info=page_info)
