import re

from graphql_relay import from_global_id

first_cap_re = re.compile("(.)([A-Z][a-z]+)")
all_cap_re = re.compile("([a-z0-9])([A-Z])")


def camel_case_to_underscore(text):
    s1 = first_cap_re.sub(r"\1_\2", text)
    return all_cap_re.sub(r"\1_\2", s1).lower()


def retrieve_id(string_id, class_name):
    object_type, object_id = from_global_id(string_id)
    if object_type != class_name:
        raise ValueError(f"Passed ID does not belong to class: {class_name}")
    return int(object_id)


def retrieve_instance(session, model, gid):
    model_name = model.__name__
    _id = retrieve_id(gid, model_name)
    if _id is None:
        raise TypeError(f"{model_name} ID: {gid} is wrong type")
    instance = session.get(ident=_id, entity=model)
    if instance is None:
        raise ValueError(f"{model_name} ID: {gid} does not exist in the db")
    return instance
