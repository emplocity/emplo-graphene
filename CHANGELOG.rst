0.12.0
======

 - remove upper bound on Werkzeug 3

0.11.0
======

 - [BREAKING CHANGE] Switch to sentry-sdk for GraphQL error reporting.
 - Add support for Python 3.11 and 3.12

0.10.6
=====

 - Bump graphene-sqlalchemy to 2.3.0
 - Change get_model_filtering_fields method

0.9.0
=====

 - Bump requirements
 - Change GrapheneModelMixin and RelayPagination typing

0.8.2
=====

 - Change GrapheneModelMixin typing

0.8.1
=====

 - Change GrapheneManagerMixin typing

0.8.0
=====

 - Drop python 3.8 support
 - Replace flake8 with ruff
 - Bump requirements
 - Code refactor with mypy

0.7.2
=====

 - Build package using pyproject.toml

0.7.1
=====

 - Log operation type and name, if available.

0.7.0
=====

 - Load logger in GraphQLEndpoint when instantiated. This helps changing
   logging level at runtime.

0.6.0
=====

 - Use converters from upstream graphene-sqlalchemy. This is a breaking
   change if queries filter by exact datetime (which is silly).

0.5.2
=====

 - fix version in __init__.py

0.5.1
=====

 - add support for Werkzeug 2.0

0.5.0
=====

 - provide initial support for Python 3.9 and SQLAlchemy 1.4

0.4.1
=====

 - fix - added graphene-file-upload explicitely to setup.py requirements

0.4.0
=====

 - provide scalar Upload functionality. More about feature: https://github.com/jaydenseric/graphql-upload

0.3.4
=====

 - pin graphene-sqlalchemy below 2.3.0
 - add tests for filtering connections

0.3.3
=====

 - pin third-party libraries to known working versions

0.3.2
=====

 - remove Protocol inheritance for GrapheneModelMixin and GrapheneManagerMixin

0.3.0
=====

 - added GrapheneModelMixin and GrapheneManagerMixin

0.2.0
=====

 - added GraphQLEndpoint class
0.1.0
=====

 - initial version ported over from emplo-utils
