from datetime import datetime
from typing import Optional

from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import configure_mappers, declarative_base, relationship

from emplo_graphene.mixins.model import GrapheneModelMixin

Base = declarative_base()


class Author(GrapheneModelMixin, Base):
    __tablename__ = "author"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Text)

    __custom_resolver_args__ = ["search", "name_length"]

    def __str__(self):
        return f"Author(name={self.name})"

    __repr__ = __str__

    @hybrid_property
    def search(self) -> str:
        return ""

    @hybrid_property
    def name_length(self) -> int:
        return 0


class Book(GrapheneModelMixin, Base):
    __tablename__ = "book"
    __filterable__ = ["created", "title"]
    __custom_resolver_args__ = ["author_name"]

    id = Column(Integer, primary_key=True, autoincrement=True)
    created = Column(DateTime, default=datetime.now())
    title = Column(Text)
    author_id = Column(
        "author_id", Integer, ForeignKey("author.id", ondelete="CASCADE")
    )
    author: Author = relationship(Author, backref="books")

    def __str__(self):
        return f"Book(title={self.title})"

    __repr__ = __str__

    @hybrid_property
    def author_name(self) -> Optional[str]:
        try:
            return self.author.name
        except AttributeError:
            return None


configure_mappers()
