import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .models import Author, Base, Book

DATABASE_URI = "sqlite://"


@pytest.fixture(scope="session")
def db_connection():
    engine = create_engine(DATABASE_URI, future=True)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    connection = engine.connect()
    yield connection
    Base.metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture
def db_session(db_connection):
    transaction = db_connection.begin()
    session = sessionmaker(bind=db_connection, future=True)
    db_session = session()
    yield db_session
    transaction.rollback()
    db_session.close()


@pytest.fixture
def author_factory(db_session):
    def _author_factory(**kwargs):
        author = Author(**kwargs)
        db_session.add(author)
        db_session.flush()
        return author

    yield _author_factory


@pytest.fixture
def book_factory(db_session):
    def _book_factory(**kwargs):
        book = Book(**kwargs)
        db_session.add(book)
        db_session.flush()
        return book

    yield _book_factory
