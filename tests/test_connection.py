from graphene import Schema

from .schema import Query


def test_book_title_author_sanity_check(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)

    query = """query Book {
        book {
            title
            author {
                name
            }
        }
    }"""
    expected = {
        "book": {
            "title": book.title,
            "author": {"name": author.name},
        }
    }
    schema = Schema(query=Query)
    result = schema.execute(query, context_value={"session": db_session})

    assert not result.errors
    assert result.data == expected


def test_filter_books_by_title(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    other_book = book_factory(title="A Farewell To Arms", author=author)

    query = """query BooksByTitle($title: String!) {
        books(title:$title) {
            edges {
                node {
                    title
                }
            }
        }
    }"""
    variables = {"title": book.title}
    expected = {"books": {"edges": [{"node": {"title": book.title}}]}}
    schema = Schema(query=Query)
    result = schema.execute(
        query, variable_values=variables, context_value={"session": db_session}
    )

    assert not result.errors
    assert result.data == expected
    titles = [edge["node"]["title"] for edge in result.data["books"]["edges"]]
    assert book.title in titles
    assert other_book.title not in titles


def _test_filter_books_by_created(db_session, author_factory, book_factory):
    # TODO: This test passes only if we use upstream converters in filtering.py
    # which *may* be a breaking change for frontend applications.
    # Such filtering is mpstly pointless anyway, as we support only
    # exact matches wich (mili?)second precision. Date ranges are possible
    # only in custom resolvers.
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    _ = book_factory(title="A Farewell To Arms", author=author)

    query = """query BooksByCreated($created: DateTime) {
        books(created:$created) {
            edges {
                node {
                    title
                }
            }
        }
    }"""
    variables = {"created": book.created.isoformat()}
    expected = {"books": {"edges": [{"node": {"title": book.title}}]}}
    schema = Schema(query=Query)
    result = schema.execute(
        query, variable_values=variables, context_value={"session": db_session}
    )

    assert not result.errors
    assert result.data == expected


def test_filter_authors_by_search(db_session, author_factory):
    _ = author_factory(name="Ernest Hemingway")
    other_author = author_factory(name="Frank Herbert")

    query = """query SearchAuthors($search: String) {
        authors(search:$search) {
            edges {
                node {
                    name
                }
            }
        }
    }"""
    variables = {"search": "erber"}
    expected = {"authors": {"edges": [{"node": {"name": other_author.name}}]}}
    schema = Schema(query=Query)
    result = schema.execute(
        query, variable_values=variables, context_value={"session": db_session}
    )

    assert not result.errors
    assert result.data == expected


def test_filter_authors_by_name_lenght(db_session, author_factory):
    _ = author_factory(name="Ernest Hemingway")
    other_author = author_factory(name="Frank Herbert")

    query = """query nameLengthAuthors($nameLength: Int) {
        authors(nameLength:$nameLength) {
            edges {
                node {
                    name
                }
            }
        }
    }"""
    variables = {"nameLength": 13}
    expected = {"authors": {"edges": [{"node": {"name": other_author.name}}]}}
    schema = Schema(query=Query)
    result = schema.execute(
        query, variable_values=variables, context_value={"session": db_session}
    )

    assert not result.errors
    assert result.data == expected


def test_order_books_by_title(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    first_book = book_factory(title="A Farewell To Arms", author=author)
    second_book = book_factory(title="For Whom The Bell Tolls", author=author)

    query = """ query SortedBooks {
        books(orderBy: "title__DESC") {
            edges {
                node {
                    title
                }
            }
        }
    }"""
    schema = Schema(query=Query)
    result = schema.execute(query, context_value={"session": db_session})

    assert not result.errors
    titles = [edge["node"]["title"] for edge in result.data["books"]["edges"]]
    assert titles == [second_book.title, first_book.title]
