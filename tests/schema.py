import graphene
from graphene import ObjectType
from graphene.relay import Node
from graphene_sqlalchemy import SQLAlchemyObjectType
from sqlalchemy.sql import func

from emplo_graphene.filtering import FilterSQLAlchemyConnectionField

from .models import Author, Book


class AuthorNode(SQLAlchemyObjectType):
    class Meta:
        model = Author
        interfaces = (Node,)


class BookNode(SQLAlchemyObjectType):
    class Meta:
        model = Book
        interfaces = (Node,)


class Query(ObjectType):
    authors = FilterSQLAlchemyConnectionField(AuthorNode)
    book = graphene.Field(BookNode)
    books = FilterSQLAlchemyConnectionField(BookNode)
    fail = graphene.String()

    def resolve_authors(self, info, search=None, name_length=None, **kwargs):
        session = info.context["session"]
        if name_length:
            return session.query(Author).filter(func.length(Author.name) == name_length)
        return session.query(Author).filter(Author.name.ilike(f"%{search}%"))

    def resolve_book(self, info, **kwargs):
        session = info.context["session"]
        return session.query(Book).first()

    def resolve_fail(self, info, **kwargs):
        raise ValueError("Failed successfully!")
