import graphene

from emplo_graphene.pagination import RelayPagination


class ExampleNode(graphene.ObjectType):
    name = graphene.String()


class ExampleConnection(graphene.relay.Connection):
    class Meta:
        node = ExampleNode


def test_relay_pagination_limit_and_offset():
    pagination = RelayPagination(
        first=10,
        after=RelayPagination.get_cursor(2),
        connection_type=ExampleConnection,
        queryset_size=80,
    )

    assert pagination.limit == 10
    assert pagination.offset == 3


def test_relay_pagination_get_connection_when_edges_should_only_has_next_page():
    nodes = [ExampleNode(name="a"), ExampleNode(name="b")]
    first = 1
    pagination = RelayPagination(
        first=first, queryset_size=len(nodes), connection_type=ExampleConnection
    )
    edges = pagination.get_connection(nodes)

    assert edges.page_info.has_next_page is True
    assert edges.page_info.has_previous_page is False
    assert pagination.limit == first
    assert pagination.offset == 0


def test_relay_pagination_get_connection_when_edges_should_only_has_previous_page():
    nodes = [ExampleNode(name="a"), ExampleNode(name="b")]
    cursor_index = 0
    first = 1
    pagination = RelayPagination(
        first=first,
        after=RelayPagination.get_cursor(cursor_index),
        queryset_size=len(nodes),
        connection_type=ExampleConnection,
    )
    edges = pagination.get_connection(nodes)

    assert edges.page_info.has_next_page is False
    assert edges.page_info.has_previous_page is True
    assert pagination.limit == first
    assert pagination.offset == cursor_index + 1


def test_relay_pagination_get_connection_when_edges_should_has_next_and_previous_page():
    nodes = [
        ExampleNode(name="a"),
        ExampleNode(name="b"),
        ExampleNode(name="c"),
        ExampleNode(name="d"),
    ]
    cursor_index = 0
    pagination = RelayPagination(
        first=2,
        after=RelayPagination.get_cursor(cursor_index),
        queryset_size=len(nodes),
        connection_type=ExampleConnection,
    )
    edges = pagination.get_connection(nodes)

    assert edges.page_info.has_next_page is True
    assert edges.page_info.has_previous_page is True
    assert pagination.limit == 2
    assert pagination.offset == cursor_index + 1
