import graphene
from sqlalchemy import and_, or_

from emplo_graphene.filtering import get_filtering_args, get_model_filtering_fields

from .models import Book


def test_get_model_filtering_fields():
    fields = get_model_filtering_fields(Book)
    assert "title" in fields
    assert isinstance(fields["title"], graphene.String)
    assert "created" in fields
    assert isinstance(fields["created"], graphene.DateTime)


def test_get_filtering_args_simple_string():
    filterable_fields = ["title"]
    filter_args = get_filtering_args(Book, filterable_fields, {"title": "Moby Dick"})
    assert filter_args[0].compare(Book.title == "Moby Dick")


def test_get_filtering_args_where_there_is_alternative_symbol_in_string():
    filterable_fields = ["title"]
    filter_args = get_filtering_args(
        Book, filterable_fields, {"title": "Moby Dick|For Whom The Bell Tolls"}
    )
    assert filter_args[0].compare(
        or_(
            Book.title == "Moby Dick",
            Book.title == "For Whom The Bell Tolls",
        )
    )


def test_get_filtering_args_where_there_is_conjuntion_symbol_in_string():
    filterable_fields = ["title"]
    filter_args = get_filtering_args(
        Book, filterable_fields, {"title": "Moby Dick&For Whom The Bell Tolls"}
    )
    assert filter_args[0].compare(
        and_(
            Book.title == "Moby Dick",
            Book.title == "For Whom The Bell Tolls",
        )
    )
