import pytest

from emplo_graphene.utils import retrieve_id, retrieve_instance

from .models import Author, Book


def test_retrieve_id(author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    instance_id = retrieve_id(book.gid, book.__class__.__name__)
    assert instance_id == book.id


def test_retrieve_id_wrong_class(author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    with pytest.raises(ValueError):
        retrieve_id(book.gid, "Author")


def test_retrieve_instance(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    instance = retrieve_instance(db_session, Book, book.gid)
    assert instance == book


def test_retrieve_instance_wrong_class(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)
    with pytest.raises(ValueError):
        retrieve_instance(db_session, Author, book.gid)
