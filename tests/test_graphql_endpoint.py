from unittest.mock import patch

from emplo.testing import GraphqlClient
from graphene import Schema

from emplo_graphene.graphql_endpoint import GraphQLEndpoint

from .schema import Query


def test_endpoint_query(db_session, author_factory, book_factory):
    author = author_factory(name="Ernest Hemingway")
    book = book_factory(title="For Whom The Bell Tolls", author=author)

    schema = Schema(query=Query)
    endpoint = GraphQLEndpoint(schema=schema)
    client = GraphqlClient(db_session, endpoint)
    query = {
        "query": """query BooksByTitle($title: String!) {
        books(title:$title) {
            edges {
                node {
                    title
                }
            }
        }
        }""",
        "variables": {"title": book.title},
    }
    result = client.send_protected_query(query, token=None)
    expected = {"books": {"edges": [{"node": {"title": "For Whom The Bell Tolls"}}]}}
    assert "errors" not in result
    assert result["data"] == expected


@patch("sentry_sdk.capture_exception")
def test_endpoint_query_syntax_error(capture_exception, db_session):
    capture_exception.return_value = "sentry-event-id"
    schema = Schema(query=Query)
    endpoint = GraphQLEndpoint(schema=schema)
    client = GraphqlClient(db_session, endpoint)
    query = {
        "query": """query BooksByTitle($title: String!) {
        books(title:$title) {
            edges {
                node {
                    title
        }""",
        "variables": {"title": "whatever"},
    }
    result = client.send_protected_query(query, token=None)
    assert "errors" in result
    assert result["errors"][0]["error_type"] == "InternalError"
    assert result["errors"][0]["sentry"] == "sentry-event-id"


@patch("sentry_sdk.capture_exception")
def test_endpoint_query_unknown_resolver_error(capture_exception, db_session):
    capture_exception.return_value = "sentry-event-id"
    schema = Schema(query=Query)
    endpoint = GraphQLEndpoint(schema=schema)
    client = GraphqlClient(db_session, endpoint)
    query = {
        "query": """query UnknownQuery {
            unknown
        }""",
    }
    result = client.send_protected_query(query, token=None)
    assert "errors" in result
    assert result["errors"][0]["error_type"] == "InternalError"
    assert result["errors"][0]["sentry"] == "sentry-event-id"


@patch("sentry_sdk.capture_exception")
def test_endpoint_query_failed_resolver_error(capture_exception, db_session):
    capture_exception.return_value = "sentry-event-id"
    schema = Schema(query=Query)
    endpoint = GraphQLEndpoint(schema=schema)
    client = GraphqlClient(db_session, endpoint)
    query = {
        "query": """query FailQuery {
            fail
        }""",
    }
    result = client.send_protected_query(query, token=None)
    assert "errors" in result
    assert result["errors"][0]["error_type"] == "ValueError"
    assert result["errors"][0]["sentry"] == "sentry-event-id"
